index.html: $(wildcard full_text/*.adoc) $(wildcard assets/*)
	asciidoctor -r asciidoctor-multipage -b multipage_html5 --out-file index.html full_text/index.adoc

tda.pdf: $(wildcard full_text/*.adoc) $(wildcard assets/*)
	asciidoctor-pdf --out-file tda.pdf full_text/index.adoc

tda_presentation.html: $(wildcard presentation/*.adoc) $(wildcard assets/*) presentation/revealjs.css
	asciidoctor-revealjs-macos --out-file tda_presentation.html presentation/index.adoc

clean:
	rm -f _*.html
	rm -f index.html
	rm -f tda.pdf
	rm -f tda_presentation.html
